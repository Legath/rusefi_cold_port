PROJECT = rusEfi

USE_VERBOSE_COMPILE=yes
# Compiler options here.
ifeq ($(USE_OPT),)
  USE_OPT = -O2 -ggdb -fomit-frame-pointer -falign-functions=16 -std=gnu99
endif

# C specific options here (added to USE_OPT).
ifeq ($(USE_COPT),)
  USE_COPT = 
endif

# C++ specific options here (added to USE_OPT).
ifeq ($(USE_CPPOPT),)
  USE_CPPOPT = -fno-rtti
endif

# Enable this if you want the linker to remove unused code and data
ifeq ($(USE_LINK_GC),)
  USE_LINK_GC = yes
endif

# Linker extra options here.
ifeq ($(USE_LDOPT),)
  USE_LDOPT = 
endif

# Enable this if you want link time optimizations (LTO)
ifeq ($(USE_LTO),)
  USE_LTO = yes
endif

# If enabled, this option allows to compile the application in THUMB mode.
ifeq ($(USE_THUMB),)
  USE_THUMB = yes
endif

# Enable this if you want to see the full log while compiling.
ifeq ($(USE_VERBOSE_COMPILE),)
  USE_VERBOSE_COMPILE = no
endif

#
# Build global options
##############################################################################

##############################################################################
# Architecture or project specific options
#

# Stack size to be allocated to the Cortex-M process stack. This stack is
# the stack used by the main() thread.
ifeq ($(USE_PROCESS_STACKSIZE),)
  USE_PROCESS_STACKSIZE = 0x400
endif

# Stack size to the allocated to the Cortex-M main/exceptions stack. This
# stack is used for processing interrupts and exceptions.
ifeq ($(USE_EXCEPTIONS_STACKSIZE),)
  USE_EXCEPTIONS_STACKSIZE = 0x400
endif

# Enables the use of FPU on Cortex-M4 (no, softfp, hard).
ifeq ($(USE_FPU),)
  USE_FPU = hard
endif

#
# Architecture or project specific options
##############################################################################

##############################################################################
#Version
VERSION_HASH=$(shell git log --pretty=format:'%h' -n 1)


# Project, sources and paths
#
ifneq ($(PROJECT_BOARD),OLIMEX_STM32_E407)
  PROJECT_BOARD = ST_STM32F4_DISCOVERY
endif
DDEFS += -D$(PROJECT_BOARD) -DVERSION="$(VERSION_HASH)"


# Imported source files and paths
#Red or Blue
ifeq ($(OS),Windows_NT)
PROJECT_DIR = .
else
PROJECT_DIR = $(shell pwd)
endif
CHIBIOS = $(PROJECT_DIR)/chibios
include $(CHIBIOS)/os/hal/hal.mk
include $(CHIBIOS)/os/hal/boards/ST_STM32F4_DISCOVERY/board.mk
include $(CHIBIOS)/os/hal/ports/STM32/STM32F4xx/platform.mk
include $(CHIBIOS)/os/hal/osal/rt/osal.mk
include $(CHIBIOS)/os/rt/rt.mk
include $(CHIBIOS)/os/rt/ports/ARMCMx/compilers/GCC/mk/port_stm32f4xx.mk
include $(CHIBIOS)/os/various/cpp_wrappers/kernel.mk
# rusEfi files
include $(PROJECT_DIR)/console/tunerstudio/tunerstudio.mk
include $(PROJECT_DIR)/hw_layer/hw_layer.mk
include $(PROJECT_DIR)/emulation/emulation.mk
include $(PROJECT_DIR)/controllers/controllers.mk
include $(PROJECT_DIR)/util/util.mk
include $(PROJECT_DIR)/config/engines/engines.mk
include $(PROJECT_DIR)/controllers/algo/algo.mk
include $(PROJECT_DIR)/controllers/core/core.mk
include $(PROJECT_DIR)/controllers/math/math.mk
include $(PROJECT_DIR)/controllers/sensors/sensors.mk
include $(PROJECT_DIR)/controllers/system/system.mk
include $(PROJECT_DIR)/controllers/trigger/trigger.mk
include $(PROJECT_DIR)/console/console.mk
include $(PROJECT_DIR)/console_util/console_util.mk
include $(PROJECT_DIR)/ext_lib/fatfs/fatfs.mk

# Define linker script file here
LDSCRIPT= $(PORTLD)/STM32F407xG.ld

# C sources that can be compiled in ARM or THUMB mode depending on the global
# setting.

#TODO: remove directly sources request
CSRC = $(PORTSRC) \
       $(KERNSRC) \
       $(TESTSRC) \
       $(HALSRC) \
       $(OSALSRC) \
       $(PLATFORMSRC) \
       $(BOARDSRC) \
	   $(UTILSRC) \
	   $(ENGINES_SRC) \
	   $(CONSOLESRC) \
	   $(TUNERSTUDIOSRC) \
	   $(CONSOLEUTILSRC) \
	   $(EMULATIONSRC) \
	   $(HW_LAYERSRC) \
	   $(CONTROLLERSSRC) \
	   $(CONTROLLERS_ALGO_SRC) \
	   $(CONTROLLERS_CORE_SRC) \
	   $(CONTROLLERS_MATH_SRC) \
	   $(CONTROLLERS_SENSORS_SRC) \
	   $(FATFSSRC) \
	   $(TRIGGER_SRC) \
	   $(SYSTEMSRC) \
	   $(CHIBIOS)/os/various/chprintf.c \
	   $(CHIBIOS)/os/various/memstreams.c

# C++ sources that can be compiled in ARM or THUMB mode depending on the global
# setting.
CPPSRC = 	$(CHCPPSRC) \
			$(TRIGGER_SRC_CPP) \
			$(TRIGGER_DECODERS_SRC_CPP) \
			$(EMULATIONSRC_CPP) \
			$(CONTROLLERS_ALGO_SRC_CPP) \
			$(SYSTEMSRC_CPP) \
			$(ENGINES_SRC_CPP) \
			$(HW_LAYER_SRC_CPP) \
			$(CONSOLE_SRC_CPP) \
			$(CONTROLLERS_SENSORS_SRC_CPP) \
			$(CONTROLLERS_SRC_CPP) \
			$(UTILSRC_CPP) \
			$(CONTROLLERS_CORE_SRC_CPP) \
			$(CONTROLLERS_MATH_SRC_CPP) \
			rusefi.cpp \
			main.cpp

# C sources to be compiled in ARM mode regardless of the global setting.
# NOTE: Mixing ARM and THUMB mode enables the -mthumb-interwork compiler
#       option that results in lower performance and larger code size.
ACSRC =

# C++ sources to be compiled in ARM mode regardless of the global setting.
# NOTE: Mixing ARM and THUMB mode enables the -mthumb-interwork compiler
#       option that results in lower performance and larger code size.
ACPPSRC =

# C sources to be compiled in THUMB mode regardless of the global setting.
# NOTE: Mixing ARM and THUMB mode enables the -mthumb-interwork compiler
#       option that results in lower performance and larger code size.
TCSRC =

# C sources to be compiled in THUMB mode regardless of the global setting.
# NOTE: Mixing ARM and THUMB mode enables the -mthumb-interwork compiler
#       option that results in lower performance and larger code size.
TCPPSRC =

# List ASM source files here
ASMSRC = $(PORTASM)

INCDIR = $(PORTINC) $(KERNINC) $(TESTINC) \
         $(HALINC) $(OSALINC) $(PLATFORMINC) $(BOARDINC) \
         $(CHIBIOS)/os/various

#
# Project, sources and paths
##############################################################################

##############################################################################
# Compiler settings
#

MCU  = cortex-m4

#TRGT = arm-elf-
TRGT ?= arm-none-eabi-
CC   = $(TRGT)gcc
CPPC = $(TRGT)g++
# Enable loading with g++ only if you need C++ runtime support.
# NOTE: You can use C++ even without C++ support if you are careful. C++
#       runtime support makes code size explode.
#LD   = $(TRGT)gcc
LD   = $(TRGT)g++
CP   = $(TRGT)objcopy
AS   = $(TRGT)gcc -x assembler-with-cpp
OD   = $(TRGT)objdump
SZ   = $(TRGT)size
HEX  = $(CP) -O ihex
BIN  = $(CP) -O binary

# ARM-specific options here
AOPT =

# THUMB-specific options here
TOPT = -mthumb -DTHUMB

# Define C warning options here
CWARN = -Wall -Wextra -Wstrict-prototypes

# Define C++ warning options here
CPPWARN = -Wall -Wextra

#
# Compiler settings
##############################################################################

##############################################################################
# Start of user section
#

# List all user C define here, like -D_DEBUG=1
UDEFS = -DCHPRINTF_USE_FLOAT=TRUE

# Define ASM defines here
UADEFS =

# List all user directories here
UINCDIR = $(PROJECT_DIR)/config \
		  $(PROJECT_DIR)/config/system \
		  $(PROJECT_DIR)/config/engines \
		  $(PROJECT_DIR)/config/boards \
	      $(PROJECT_DIR)/chibios/os/various \
	      $(PROJECT_DIR)/ext_algo/uthash/src \
	      $(PROJECT_DIR)/ext_lib/fatfs \
	      $(PROJECT_DIR)/util \
	      $(PROJECT_DIR)/console_util \
	      $(PROJECT_DIR)/console \
	      $(PROJECT_DIR)/console/tunerstudio \
	      $(PROJECT_DIR)/hw_layer \
	      $(PROJECT_DIR)/hw_layer/serial_over_usb \
	      $(PROJECT_DIR)/hw_layer/algo \
	      $(PROJECT_DIR)/hw_layer/lcd \
	      $(PROJECT_DIR)/emulation \
	      $(PROJECT_DIR)/emulation/hw_layer \
	      $(PROJECT_DIR)/emulation/test \
	      $(PROJECT_DIR)/controllers \
	      $(PROJECT_DIR)/controllers/sensors \
	      $(PROJECT_DIR)/controllers/system \
	      $(PROJECT_DIR)/controllers/algo \
	      $(PROJECT_DIR)/controllers/core \
	      $(PROJECT_DIR)/controllers/math \
	      $(PROJECT_DIR)/controllers/trigger

# List the user directory to look for the libraries here
ULIBDIR =

# List all user libraries here
ULIBS =

#
# End of user defines
##############################################################################

RULESPATH = $(CHIBIOS)/os/common/ports/ARMCMx/compilers/GCC
include $(RULESPATH)/rules.mk
