/**
 * @file    ego.h
 * @brief
 *
 *
 * @date Nov 15, 2013
 * @author Andrey Belomutskiy, (c) 2012-2014
 */

#ifndef EGO_H_
#define EGO_H_

#include "main.h"

float getAfr(void);

#endif
